# mtm Winter 1617a public repository #

This is a public repository for course "Matam" (Introduction for system programming 234122)  homework files, Winter 17 semester.  

### Instructions For Students of the course ###

The instructions below will guide you on how to:

1. Clone the repository in to your t2 server:
	thorugh the terminal you will hold a copy of this repository, and by that         you will have the files necessary in order to start your homework assignments.

2. Sync the repository in your t2 server when the remote repository updates.

	#### Clone the repository in to your t2 server ####
	1. Connect to your t2 server with the an ssh client. Instructions can be found here :
      [https://webcourse.cs.technion.ac.il/234122/Winter20162017/ho/WCFiles/Connecting_to_t2.pdf](Link URL)
	
	2. Create a new directory called mtm in your home directory and go into it:
		  
                mkdir mtm
                cd ~/mtm
                
	3. Type the following command:
		
                git clone https://mtm1617a@bitbucket.org/mtm1617a/public.git
		
		expected output :

                Initialized empty Git repository in
                /homet2/your.user.name/mtm/public/.git/
                remote: Counting objects: xx, done.
                remote: Compressing objects: 100% (xx/xx), done.
                remote: Total xx (delta 1), reused 0 (delta 0)
                Unpacking objects: 100% (xx/xx), done.

	 4. Thats it. now you have a local repostory in your t2 server with the neccesery files for your 
	 	homework assaignments.
	#####   How To Sync the repository in your t2 server when the remote repoistory updates ####
          In your T2 server navigate to the repository location :
                
                cd ~/mtm/public
		
          Type the following command :
		
		git pull origin master
	
### Author ###

* Tomer Golany - TA in charge