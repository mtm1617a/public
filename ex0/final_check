#!/bin/bash

function print_usage {
    echo "Usage:"
    echo "mtmfinal_check <submission file>"
}

# TMP_DIR_WITH_PATH will be set later by create_empty_tmp_dir 
TMP_DIR_WITH_PATH="/tmp/TO_BE_SET_LATER"
TMP_DIR_WITH_PATH_IS_SET=0

#REMEMBER:  '~' does not expand inside regular double quotes.
ORIGINAL_AUX_FILES_HOME_DIR=~mtm
ORIGINAL_AUX_FILES_BASE_DIR_PATH="$ORIGINAL_AUX_FILES_HOME_DIR/public/1415b/ex0"

# ALL_WENT_WELL_SO_FAR will be set to 0 as soon as something fails along the check (something not that critical - that we still want to continue with the check instead of callingfail_and_exit, just so that the user can see the results of the rest of the check. 
ALL_WENT_WELL_SO_FAR=1


function main {
    if (( $# != 1 )); then
        print_usage
        exit 1
    fi
    
    if [[ "$1" == "--help" ]]; then
        print_usage
        exit 1
    fi    

	
    verify_submission_file_name_and_existence "$1"
    create_empty_tmp_dir
	
    init_existing_empty_temp_dir "$1"
    
	
	echo "This script will now compile your program and run it with the published tests."
	read -p "Continue? (y/n)" -r REPLY
    if [[ ! "$REPLY" =~ ^[Yy]$ ]]; then
        fail_and_exit
    fi
	
	echo "Compiling part1..."
	compile "$TMP_DIR_WITH_PATH/part1" "$TMP_DIR_WITH_PATH/submitted_files/part1.c"
	result=$?
	if (( $result == 0 )); then
		echo "Running tests for part1:"
		run_tests "$TMP_DIR_WITH_PATH/part1" "$ORIGINAL_AUX_FILES_BASE_DIR_PATH/part1" 4 3 1
	fi
	
	echo "Compiling mtm_buggy..."
	compile "$TMP_DIR_WITH_PATH/mtm_buggy" "$TMP_DIR_WITH_PATH/submitted_files/mtm_buggy.c"
	result=$?
	if (( $result == 0 )); then
	echo "Running tests for mtm_buggy:"
		run_tests "$TMP_DIR_WITH_PATH/mtm_buggy" "$ORIGINAL_AUX_FILES_BASE_DIR_PATH/part2" 2 3 1
	fi
	
	delete_tmp_dir
	
	if (( ALL_WENT_WELL_SO_FAR == 1 )); then
		echo "Everything went well :)"
		echo "Final check passed."
	else
		echo "The final check did not fully succeed (see errors)."
		echo "Please correct your solution and try again."
		fail_and_exit
	fi
	
}


# Set up the TMP_DIR_WITH_PATH directory.
# Pre-condition: TMP_DIR_WITH_PATH exists and is empty.
# Calls fail_and_exit when fails.
# param 1: the submitted zip file (with path).
#Calls fail_and_exit when fails.
function init_existing_empty_temp_dir {
    if [ ! -d "$ORIGINAL_AUX_FILES_BASE_DIR_PATH" ]; then
        echo "ERROR: Couldn't find the following directory: $ORIGINAL_AUX_FILES_BASE_DIR_PATH"
        echo "Make sure you're running this script on T2."
        fail_and_exit
    fi
    
    ##### Creating [${TMP_DIR_WITH_PATH}/submitted_files]: 
    
    #unzip-ing:
    mkdir "${TMP_DIR_WITH_PATH}/submitted_files"
    unzip "$1" -d "${TMP_DIR_WITH_PATH}/submitted_files" > /dev/null
	result=$?
    if (( result != 0 )); then
        echo "ERROR: failed to unzip '$1'. Make sure that file is a valid zip file"
        fail_and_exit
    fi
    
    #making sure the exact expected file tree was unzipped:
    files_num=$(ls -a "${TMP_DIR_WITH_PATH}/submitted_files" | wc -l)
    number_of_files_expected=2
    if (( files_num != ($number_of_files_expected + 2) )); then
        echo "ERROR: '$1' does not contain exactly $number_of_files_expected files! The files detected are the following:"
        ls ${TMP_DIR_WITH_PATH}/submitted_files
    fi
    check_unzipped_proper_file_exists "${TMP_DIR_WITH_PATH}/submitted_files" "part1.c"
    check_unzipped_proper_file_exists "${TMP_DIR_WITH_PATH}/submitted_files" "mtm_buggy.c"

}







##############################################################
# Code that needs no updating.
##############################################################


PARENT_DIR_FOR_TEMP_DIR="/tmp"

#Verifies the submission file name ends with ".zip", and that it exists.
#Calls fail_and_exit when fails.
#param 1: the submitted zip file (with path).
function verify_submission_file_name_and_existence {
    
    if [[ "$1" != *.zip ]]; then
        echo "ERROR: your submission file must be a zip file (its name must end with '.zip')."
        echo "The file you provided ($1) isn't."
        fail_and_exit
    fi
    
    if [[ ! -f "$1" ]]; then
        echo "ERROR: The file you provided ($1) doesn't exist."
        fail_and_exit 
    fi
    
}


#param 1: timeout (in seconds; integer).
#param 2,...: command & parameters
#the exit status is:
#   1 - in case a timeout occurred before the command finished.
#   2 - in case no timeout occurred, but the command's exit status was non-zero (or other failure occurred).
#   3 - in case segfault occured.
#   0 - in case no timeout occurred, and the command's exit status was zero.
function run_with_timeout {
	TIME_OUT_EXIT_STATUS=124

    timeout "$@"
    result=$?
    
	if (( result == TIME_OUT_EXIT_STATUS )); then
        return 1
    elif (( result == 0 )); then
        return 0
	elif (( result == 139 )); then
		return 3
    else
        return 2
    fi
}



#Creates an empty temporary directory. Sets TMP_DIR_WITH_PATH to be the full name of the new temporary directory.
#calls fail_and_exit when failed.
function create_empty_tmp_dir {

    temp_dir_with_path=$(mktemp -d "$PARENT_DIR_FOR_TEMP_DIR/TMP__XXXXXXXXXXXX")
    result=$?
	if (( result != 0 )); then
		echo "ERROR: Failed to create temporary directory under '$PARENT_DIR_FOR_TEMP_DIR'!"
		fail_and_exit
	fi
	
    if [ ! -d "$temp_dir_with_path" ]; then
        echo "ERROR: Failed to create temporary directory '$temp_dir_with_path'"
        fail_and_exit
    fi
	
	TMP_DIR_WITH_PATH="$temp_dir_with_path"
	TMP_DIR_WITH_PATH_IS_SET=1
}

#Deletes the directory TMP_DIR_WITH_PATH.
#Note: in order to prevent infinite loop, this function does not call fail_and_exit on any event.
function delete_tmp_dir {

	if (( TMP_DIR_WITH_PATH_IS_SET == 0 )); then
		echo "ERROR: TMP_DIR_WITH_PATH isn't set."
		return
	fi
	
	if [ ! -d "$TMP_DIR_WITH_PATH" ]; then
        echo "ERROR: The directory $TMP_DIR_WITH_PATH doesn't exist."
		return
    fi
	
	if [[ $(echo "$TMP_DIR_WITH_PATH" | cut -c 1-5) != "/tmp/" ]]; then
		echo "ERROR: TMP_DIR_WITH_PATH=$TMP_DIR_WITH_PATH, and it supposes to begin with '/tmp/'!"
		return
	else
		rm -rf "$TMP_DIR_WITH_PATH"
		TMP_DIR_WITH_PATH_IS_SET=0
	fi
	
	if [ -d "$TMP_DIR_WITH_PATH" ]; then
        echo "ERROR: Failed to delete temporary directory $TMP_DIR_WITH_PATH"
		return
    fi
}

#Copies a file/directory safely (no overwrite occurs). 
#calls fail_and_exit when failed.
#param 1: source
#param 2: target
function safe_copy {
    if [[ -e $2 ]]; then
        echo "ERROR: Can't copy '$1' to '$2' because '$2' already exists!"
        fail_and_exit
    fi
    
    if [[ ! -e $1 ]]; then
        echo "ERROR: Can't copy '$1' to '$2' because '$1' doesn't exist!"
        fail_and_exit
    fi
    
    cp -r "$1" "$2"
    
   if [[ ! -e $2 ]]; then
        echo "ERROR: Coping '$1' to '$2' failed!"
        fail_and_exit
    fi 
}

# Checks that a proper file (not a directory) we've expected to unzipped indeed exists.
# Calls fail_and_exit if fails (if file does not exists)
#param 1: the directory to which the zip file was unzipped (including path).
#param 2: the proper file to make sure exists, including path - relative to $1.
function check_unzipped_proper_file_exists {
    if [[ ! -f "$1/$2" ]]; then
        echo "ERROR: The following file must be in your zip file, and is missing:"
        echo "$2"
        fail_and_exit
    fi
}

# Checks that a directory we've expected to unzipped indeed exists.
# Calls fail_and_exit if fails (if directory does not exists)
#param 1: the directory to which the zip file was unzipped (including path).
#param 2: the directory to make sure exists, including path - relative to $1.
function check_unzipped_dir_exists {
    if [[ ! -d "$1/$2" ]]; then
        echo "ERROR: The following directory must be in your zip file, and is missing:"
        echo "$2"
        fail_and_exit
    fi
}


# Compiles a given set of c files.
# Calls fail_and_exit when failing in an unexpected way.
# Sets ALL_WENT_WELL_SO_FAR to 0 if compilation failed.
# Returns 0 on success, and 1 if compilation failed.
# param1 - the output executable to create (with path).
# param2,... - the c source files to compile (with path)
function compile {
	if [[ -e $1 ]]; then
		echo "ERROR: Can't compile! target file already exists."
		fail_and_exit
	fi
	
	gcc -std=c99 -Wall -pedantic-errors -Werror -DNDEBUG -lm -o "$@" 2>/dev/null
	result=$?
	compilation_success=1
	if [[ ( $result -ne 0  ) || ( ! -f $1 ) ]]; then
		echo "ERROR: Compiling error occurred. Try compiling yourself with the flags:"
		echo "gcc -std=c99 -Wall -pedantic-errors -Werror -DNDEBUG -lm ..."
		ALL_WENT_WELL_SO_FAR=0
		return 1
	fi
	return 0
}

# Runs test with a given executable.
# Sets ALL_WENT_WELL_SO_FAR to 0 if at least one test failed.
# Calls fail_and_exit when failing in an unexpected way.
# All the temporary test output files are saved under ["$TMP_DIR_WITH_PATH/test-outputs"] which is created and then deleted by this function.
# param 1 - the executable file.
# param 2 - the directory containing the test files, in the format: test<number>.in, test<number>.out, test<number>.err (the err file is optional, if it doesn't exist - then we require no std error) where <number> starts from 1.
# param 3 - the number of tests to run.
# param 4 - timeout (in seconds, integer) to limit each test (if it doesn't finish within this time span - it fails).
# param 5 - either 0 or 1. If 1, then the exit code of the executable must be 0, or else we consider it failed. If 0, then the exit code of the executable doesn't matter.

function run_tests {
	
	if (( TMP_DIR_WITH_PATH_IS_SET == 0 )); then
		echo "ERROR: TMP_DIR_WITH_PATH isn't set."
		fail_and_exit
	fi
	
	test_outputs_dir="$TMP_DIR_WITH_PATH/test-outputs"
	mkdir "$test_outputs_dir"
	
	for i in $(seq 1 $3); do
		test_ok=1
		echo -n "Running test $i: "
		run_with_timeout $4 "$1" <"$2/test$i.in" 1>"$test_outputs_dir/test$i.out" 2>"$test_outputs_dir/test$i.err"

		result=$?
		if (( result == 1 )); then
			echo "FAILED!!!!! (timeout)"
			test_ok=0
		elif ((  result == 3 )); then
			echo "FAILED!!!!! (segfault)"
			test_ok=0
		elif ((  (result == 2 ) && ( $5 == 1 )  )); then
			echo "FAILED!!!!! (bad exit code)"
			test_ok=0
		else
			#The executable finished ok. Lets check the output & error.
			diff_wc=$(diff "$2/test$i.out" "$test_outputs_dir/test$i.out" | wc -l)
			if (( diff_wc > 0 )); then
				echo "FAILED!!!!! (bad output - 'diff' reported differences)"
				test_ok=0
			else
				# Lets check the std error.
				
				if [[ -f "$2/test$i.err" ]]; then
					diff_wc=$(diff "$2/test$i.err" "$test_outputs_dir/test$i.err" | wc -l)
					if (( diff_wc > 0 )); then
						echo "FAILED!!!!! (bad std error - 'diff' reported differences)"
						test_ok=0
					fi
				else
					error_wc=$(cat "$test_outputs_dir/test$i.err" | wc -l)
					if (( error_wc > 0 )); then
						echo "FAILED!!!!! (bad std error - was suppose to be empty!)"
						test_ok=0
					fi
				fi
			fi	
		fi
		
		if (( test_ok == 1 )); then
			echo "success"
		else
			ALL_WENT_WELL_SO_FAR=0
		fi
			
	done
	
	if [[ $(echo "$test_outputs_dir" | cut -c 1-5) != "/tmp/" ]]; then
		echo "ERROR: test_outputs_dir=$test_outputs_dir, and it supposes to begin with '/tmp/'!"
		fail_and_exit
	else
		rm -rf "$test_outputs_dir"
	fi
}



function fail_and_exit {
    echo "****   The final check failed   ****"
	
	if (( TMP_DIR_WITH_PATH_IS_SET == 1 )); then
		delete_tmp_dir
	fi
	
    exit 1
}

main "$@"
    
    
